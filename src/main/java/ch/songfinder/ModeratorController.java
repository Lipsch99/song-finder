package ch.songfinder;

import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

import static ch.songfinder.jooq.model.tables.Role.ROLE;
import static ch.songfinder.jooq.model.tables.Song.SONG;
import static ch.songfinder.jooq.model.tables.User.USER;
import static ch.songfinder.jooq.model.tables.Vote.VOTE;
import static org.jooq.impl.DSL.count;

@CrossOrigin(origins = "*")
@RestController
public class ModeratorController {
    private final DSLContext dbContext;

    @Autowired
    public ModeratorController(DSLContext dbContext) {
        this.dbContext = dbContext;
    }

    @Secured({"ROLE_MODERATOR"})
    @GetMapping("/users")
    public ResponseEntity<List<User>> users() {
        List<User> users = dbContext.select(USER.NAME.as("username"), ROLE.NAME.as("role"))
                .from(USER)
                .leftJoin(ROLE).on(USER.ROLE.eq(ROLE.ID))
                .fetchInto(User.class);

        return ResponseEntity.status(200).body(users);
    }

    @Secured({"ROLE_MODERATOR"})
    @GetMapping("/allSongs")
    public ResponseEntity getAllSongs() {
        List<Song> songs = dbContext.select(SONG.NAME, SONG.SPOTIFY_LINK, SONG.ACCEPTED)
                .from(SONG)
                .fetchInto(Song.class);

        return ResponseEntity.status(200).body(songs);
    }

    @Secured({"ROLE_MODERATOR"})
    @PostMapping("/acceptSong")
    public ResponseEntity acceptSong(@Valid @RequestBody Song song) {
        dbContext.update(SONG)
                .set(SONG.ACCEPTED, true)
                .where(SONG.NAME.eq(song.name))
                .execute();

        return ResponseEntity.status(200).build();
    }

    @Secured({"ROLE_MODERATOR"})
    @PostMapping("/deleteSong")
    public ResponseEntity deleteSong(@Valid @RequestBody Song song) {
        dbContext.delete(SONG)
                .where(SONG.NAME.eq(song.name))
                .execute();

        return ResponseEntity.status(200).build();
    }

    @Secured({"ROLE_MODERATOR"})
    @GetMapping("/songs/mostVoted")
    public ResponseEntity getMostVotedSongs() {
        List<SongVote> songs = dbContext.select(SONG.NAME,
                count(DSL.nullif(true, VOTE.POSITIVE_VOTE)).as("Likes"),
                count(DSL.nullif(false, VOTE.POSITIVE_VOTE)).as("Dislikes"))
                .from(VOTE)
                .leftJoin(SONG).on(VOTE.SONG_ID.eq(SONG.ID))
                .groupBy(SONG.NAME)
                .orderBy(count().desc())
                .limit(10)
                .fetchInto(SongVote.class);

        return ResponseEntity.status(200).body(songs);
    }

    @Secured({"ROLE_MODERATOR"})
    @GetMapping("/songs/bestRated")
    public ResponseEntity getBestRatedSongs() {
        List<SongVote> songs = dbContext.select(SONG.NAME,
                count(DSL.nullif(true, VOTE.POSITIVE_VOTE)).as("Likes"),
                count(DSL.nullif(false, VOTE.POSITIVE_VOTE)).as("Dislikes"))
                .from(VOTE)
                .leftJoin(SONG).on(VOTE.SONG_ID.eq(SONG.ID))
                .groupBy(SONG.NAME)
                .orderBy(count(DSL.nullif(true, VOTE.POSITIVE_VOTE)).add(1)
                        .divide(count(DSL.nullif(false, VOTE.POSITIVE_VOTE)).add(1))
                        .desc())
                .limit(10)
                .fetchInto(SongVote.class);

        return ResponseEntity.status(200).body(songs);
    }

    public static class Song {
        @NotBlank(message = "Name is empty.")
        private String name;
        private String spotifyLink;
        private boolean accepted;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSpotifyLink() {
            return spotifyLink;
        }

        public void setSpotifyLink(String spotifyLink) {
            this.spotifyLink = spotifyLink;
        }

        public boolean isAccepted() {
            return accepted;
        }

        public void setAccepted(boolean accepted) {
            this.accepted = accepted;
        }
    }

    public static class SongVote {
        private String name;
        private int likes;
        private int dislikes;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getLikes() {
            return likes;
        }

        public void setLikes(int likes) {
            this.likes = likes;
        }

        public int getDislikes() {
            return dislikes;
        }

        public void setDislikes(int dislikes) {
            this.dislikes = dislikes;
        }
    }

    public static class User {
        public String username;
        public String role;
    }

    public static class UserName {
        @NotBlank(message = "Enter a username!")
        public String username;
    }

}
