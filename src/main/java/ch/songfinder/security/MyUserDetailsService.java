package ch.songfinder.security;

import ch.songfinder.StaticMethods;
import ch.songfinder.jooq.model.tables.records.UserRecord;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static ch.songfinder.jooq.model.tables.User.USER;
import static ch.songfinder.jooq.model.tables.Role.ROLE;

@Service
public class MyUserDetailsService implements UserDetailsService {

    private final DSLContext dbContext;

    @Autowired
    public MyUserDetailsService(DSLContext dbContext) {
        this.dbContext = dbContext;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        UserRecord userRecord = LoadUserFromDatabase(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));

        return new UserDetails(userRecord, dbContext);
    }

    private Optional<UserRecord> LoadUserFromDatabase(String username) {
        return dbContext.selectFrom(USER).where(USER.NAME.eq(username)).fetchOptional();
    }


    private static class UserDetails implements org.springframework.security.core.userdetails.UserDetails {

        private UserRecord userRecord;
        private DSLContext dbContext;


        public UserDetails(UserRecord userRecord, DSLContext dbContext) {
            this.userRecord = userRecord;
            this.dbContext = dbContext;
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
            switch (userRecord.getRole()) {
                case 3:
                    authorities.add(new SimpleGrantedAuthority("ROLE_ADMINISTRATOR"));
                case 2:
                    authorities.add(new SimpleGrantedAuthority("ROLE_MODERATOR"));
                    break;
            }
            return authorities;
        }


        @Override
        public String getPassword() {
            return userRecord.getPassword();
        }

        @Override
        public String getUsername() {
            return userRecord.getName();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }
}
