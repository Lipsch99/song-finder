package ch.songfinder;

import ch.songfinder.jooq.model.tables.records.UserRecord;
import org.jooq.DSLContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

import static ch.songfinder.jooq.model.tables.User.USER;

public class StaticMethods {

    public static UserRecord getAuthenticatedUser(DSLContext dbContext) {
        String username = ((UserDetails) SecurityContextHolder.getContext().
                getAuthentication().getPrincipal()).getUsername();

        return getUserRecordByUsername(dbContext, username);
    }

    public static UserRecord getUserRecordByUsername(DSLContext dbContext, String username){
        Optional<UserRecord> user =
                dbContext.selectFrom(USER)
                        .where(USER.NAME.eq(username))
                        .fetchOptional();

        return user.orElseThrow(() -> new RuntimeException("user: " + username + " was not found."));
    }
}
