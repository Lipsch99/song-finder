/*
 * This file is generated by jOOQ.
 */
package ch.songfinder.jooq.model.tables.records;


import ch.songfinder.jooq.model.tables.Vote;

import java.time.LocalDate;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VoteRecord extends UpdatableRecordImpl<VoteRecord> implements Record4<Integer, Integer, Boolean, LocalDate> {

    private static final long serialVersionUID = -945670572;

    /**
     * Setter for <code>public.Vote.user_id</code>.
     */
    public void setUserId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>public.Vote.user_id</code>.
     */
    public Integer getUserId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>public.Vote.song_id</code>.
     */
    public void setSongId(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>public.Vote.song_id</code>.
     */
    public Integer getSongId() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>public.Vote.positive_vote</code>.
     */
    public void setPositiveVote(Boolean value) {
        set(2, value);
    }

    /**
     * Getter for <code>public.Vote.positive_vote</code>.
     */
    public Boolean getPositiveVote() {
        return (Boolean) get(2);
    }

    /**
     * Setter for <code>public.Vote.last_voted</code>.
     */
    public void setLastVoted(LocalDate value) {
        set(3, value);
    }

    /**
     * Getter for <code>public.Vote.last_voted</code>.
     */
    public LocalDate getLastVoted() {
        return (LocalDate) get(3);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row4<Integer, Integer, Boolean, LocalDate> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    @Override
    public Row4<Integer, Integer, Boolean, LocalDate> valuesRow() {
        return (Row4) super.valuesRow();
    }

    @Override
    public Field<Integer> field1() {
        return Vote.VOTE.USER_ID;
    }

    @Override
    public Field<Integer> field2() {
        return Vote.VOTE.SONG_ID;
    }

    @Override
    public Field<Boolean> field3() {
        return Vote.VOTE.POSITIVE_VOTE;
    }

    @Override
    public Field<LocalDate> field4() {
        return Vote.VOTE.LAST_VOTED;
    }

    @Override
    public Integer component1() {
        return getUserId();
    }

    @Override
    public Integer component2() {
        return getSongId();
    }

    @Override
    public Boolean component3() {
        return getPositiveVote();
    }

    @Override
    public LocalDate component4() {
        return getLastVoted();
    }

    @Override
    public Integer value1() {
        return getUserId();
    }

    @Override
    public Integer value2() {
        return getSongId();
    }

    @Override
    public Boolean value3() {
        return getPositiveVote();
    }

    @Override
    public LocalDate value4() {
        return getLastVoted();
    }

    @Override
    public VoteRecord value1(Integer value) {
        setUserId(value);
        return this;
    }

    @Override
    public VoteRecord value2(Integer value) {
        setSongId(value);
        return this;
    }

    @Override
    public VoteRecord value3(Boolean value) {
        setPositiveVote(value);
        return this;
    }

    @Override
    public VoteRecord value4(LocalDate value) {
        setLastVoted(value);
        return this;
    }

    @Override
    public VoteRecord values(Integer value1, Integer value2, Boolean value3, LocalDate value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VoteRecord
     */
    public VoteRecord() {
        super(Vote.VOTE);
    }

    /**
     * Create a detached, initialised VoteRecord
     */
    public VoteRecord(Integer userId, Integer songId, Boolean positiveVote, LocalDate lastVoted) {
        super(Vote.VOTE);

        set(0, userId);
        set(1, songId);
        set(2, positiveVote);
        set(3, lastVoted);
    }
}
