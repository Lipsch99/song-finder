package ch.songfinder;

import ch.songfinder.jooq.model.tables.records.VoteRecord;
import org.hibernate.validator.constraints.URL;
import org.jooq.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.*;

import static ch.songfinder.StaticMethods.getAuthenticatedUser;
import static ch.songfinder.jooq.model.tables.User.USER;
import static ch.songfinder.jooq.model.tables.Song.SONG;
import static ch.songfinder.jooq.model.tables.Vote.VOTE;

@CrossOrigin(origins = "*")
@RestController
public class UserController {

    private final DSLContext dbContext;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserController(DSLContext dbContext, PasswordEncoder passwordEncoder) {
        this.dbContext = dbContext;
        this.passwordEncoder = passwordEncoder;
    }

    @Secured({"ROLE_USER"})
    @PostMapping("/password")
    public ResponseEntity password(@Valid @RequestBody UserController.PasswordData passwordData) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String username = "";
        String password = getAuthenticatedUser(dbContext).getPassword();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
         throw new RuntimeException("User not authenticated.");
        }

        if (passwordEncoder.matches(passwordData.oldPassword, password)) {
            dbContext.update(USER)
                    .set(USER.PASSWORD, passwordEncoder.encode(passwordData.newPassword))
                    .where(USER.NAME.eq(username))
                    .execute();


            return ResponseEntity.ok().build();
        }
        PublicController.Message returnMessage = new PublicController.Message();
        returnMessage.message = "Original password was wrong.";

        return ResponseEntity.status(401).body(returnMessage);
    }

    @Secured({"ROLE_USER"})
    @PutMapping("/songs")
    public ResponseEntity suggestSong(@Valid @RequestBody SongSuggestion songSuggestion) {

        dbContext.insertInto(SONG,
                SONG.NAME, SONG.SPOTIFY_LINK)
                .values(songSuggestion.songName, songSuggestion.spotifyLink)
                .execute();

        return ResponseEntity.created(null).build();
    }

    @Secured({"ROLE_USER"})
    @GetMapping("/songs")
    public ResponseEntity getSongs() {
        Condition oldVoteCondition = VOTE.LAST_VOTED.lessOrEqual(LocalDate.now().minusMonths(1))
                .or(VOTE.LAST_VOTED.isNull());
        List<Song> songs = dbContext.select(SONG.NAME, SONG.SPOTIFY_LINK)
                .from(SONG)
                .leftJoin(VOTE)
                .on(SONG.ID.eq(VOTE.SONG_ID),VOTE.USER_ID.eq(getAuthenticatedUser(dbContext).getId()))
                .where(SONG.ACCEPTED.isTrue()
                        .and(oldVoteCondition))
                .fetchInto(Song.class);

        return ResponseEntity.status(200).body(songs);
    }

    @Secured({"ROLE_USER"})
    @PostMapping("/rateSong")
    public ResponseEntity rateSong(@Valid @RequestBody SongRating songRating){

        Record1<Integer> songIdRecord = dbContext.select(SONG.ID)
                .from(SONG)
                .where(SONG.NAME.eq(songRating.songName))
                .fetchOne();
        Integer songId = songIdRecord.value1();


        Optional<VoteRecord> vote = dbContext.selectFrom(VOTE)
                .where(VOTE.USER_ID.eq(getAuthenticatedUser(dbContext).getId())
                        .and(VOTE.SONG_ID.eq(songId)))
                .fetchOptional();

        if (vote.isPresent()){
            dbContext.update(VOTE)
                    .set(VOTE.POSITIVE_VOTE, songRating.positiveRating)
                    .set(VOTE.LAST_VOTED, LocalDate.now())
                    .where(VOTE.USER_ID.eq(getAuthenticatedUser(dbContext).getId())
                            .and(VOTE.SONG_ID.eq(songId)))
                    .execute();
        }else{
            dbContext.insertInto(VOTE,
                    VOTE.SONG_ID, VOTE.USER_ID, VOTE.POSITIVE_VOTE, VOTE.LAST_VOTED)
                    .values(songId,getAuthenticatedUser(dbContext).getId(),songRating.positiveRating, LocalDate.now())
                    .execute();
        }

        return ResponseEntity.status(200).build();
    }

    @Secured({"ROLE_USER"})
    @GetMapping("/role")
    public ResponseEntity getRole() {

        Integer roleId = getAuthenticatedUser(dbContext).getRole();

        Role userRole = new Role();
        userRole.setRoleId(roleId);
        return ResponseEntity.ok(userRole);
    }

    public static class Role{
        private Integer roleId;

        public Integer getRoleId() {
            return roleId;
        }

        public void setRoleId(Integer roleId) {
            this.roleId = roleId;
        }
    }

    public static class SongRating{
        @NotBlank(message = "Song name can not be empty.")
        private String songName;
        private boolean positiveRating;

        public void setPositiveRating(boolean positiveRating) {
            this.positiveRating = positiveRating;
        }

        public void setSongName(String songName) {
            this.songName = songName;
        }
    }

    public static class Song {
        private String name;
        private String spotifyLink;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSpotifyLink() {
            return spotifyLink;
        }

        public void setSpotifyLink(String spotifyLink) {
            this.spotifyLink = spotifyLink;
        }
    }

    public static class PasswordData {
        @NotBlank(message = "Set a new password!")
        public String oldPassword;
        @NotBlank(message = "Set a new password!")
        public String newPassword;
    }

    public static class SongSuggestion {
        @NotBlank(message = "The name can't be empty")
        public String songName;
        @NotBlank(message = "The Spotify Link can't be empty")
        @URL(message = "The Spotify Link needs to be an URL")
        public String spotifyLink;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorObj handleValidationExceptions(
            MethodArgumentNotValidException ex) {

        ErrorObj asd = new ErrorObj();
        asd.message = ex.getBindingResult().getAllErrors().get(0).getDefaultMessage();
        return asd;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(RuntimeException.class)
    public ErrorObj handleRuntimeException(
            RuntimeException ex) {

        ErrorObj error = new ErrorObj();
        error.message = "An error occurred: " + ex.getMessage();
        return error;
    }

    public static class ErrorObj {
        public String message;
    }
}
