package ch.songfinder;

import ch.songfinder.jooq.model.tables.records.UserRecord;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.Optional;

import static ch.songfinder.jooq.model.tables.User.USER;


@CrossOrigin(origins = "*")
@RestController
public class PublicController {

    private final DSLContext dbContext;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public PublicController(DSLContext dbContext, PasswordEncoder passwordEncoder) {
        this.dbContext = dbContext;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/register")
    public ResponseEntity register(RequestEntity<RegistrationData> registrationData) {

        RegistrationData data = registrationData.getBody();

        Optional<UserRecord> existingUser = dbContext.selectFrom(USER)
                .where(USER.NAME.eq(data.username))
                .fetchOptionalInto(USER);

        if (existingUser.isPresent()) {
            Message returnMessage = new Message();
            returnMessage.message = "Username is already taken.";
            return ResponseEntity.status(409).body(returnMessage);
        }

        dbContext.insertInto(USER,
                USER.NAME, USER.PASSWORD)
                .values(data.username, passwordEncoder.encode(data.password))
                .execute();



        return ResponseEntity.created(URI.create("")).build();
    }

    public static class Model {

        public String bla = "En String";
        public String foo = "FOO";
    }

    public static class RegistrationData {
        public String username;
        public String password;
    }

    public static class Message {
        public String message;
    }
}

/* Some sql queries */
//        Result<Record1<String>> result = dbContext.select(SONGS.NAME)
//                .from(SONGS)
//                .where(SONGS.ID.eq(1))
//                .fetch();
//
//        Result<SongsRecord> fetch = dbContext.selectFrom(SONGS)
//                .where(SONGS.ID.eq(1))
//                .fetch();
//
//        List<SongsRecord> songsRecords = dbContext.selectFrom(SONGS)
//                .where(SONGS.ID.eq(1))
//                .fetchInto(SongsRecord.class);
//
//        songsRecords.get(0).getId();
//
//        SongsRecord songsRecord = dbContext.selectFrom(SONGS)
//                .where(SONGS.ID.eq(1))
//                .fetchOneInto(SongsRecord.class);
//
//        Optional<SongsRecord> songsRecord1 = dbContext.selectFrom(SONGS)
//                .where(SONGS.ID.eq(1))
//                .fetchOptionalInto(SongsRecord.class);


//        Optional<String> s = dbContext.select(SONGS.NAME)
//                .from(SONGS)
//                .where(SONGS.ID.eq(1))
//                .fetchOptionalInto(String.class);


//s.orElseThrow();
