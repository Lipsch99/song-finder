package ch.songfinder;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Random;

import ch.songfinder.StaticMethods;
import static ch.songfinder.jooq.model.tables.User.USER;
import static ch.songfinder.jooq.model.tables.Role.ROLE;

@CrossOrigin(origins = "*")
@RestController
public class AdministratorController {
    private final DSLContext dbContext;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AdministratorController(DSLContext dbContext, PasswordEncoder passwordEncoder) {
        this.dbContext = dbContext;
        this.passwordEncoder = passwordEncoder;
    }

    @Secured({"ROLE_ADMINISTRATOR"})
    @PostMapping("/resetPassword")
    public ResponseEntity resetPassword(@Valid @RequestBody ModeratorController.UserName userName) {

        String randomPassword = getRandomPassword();
        dbContext.update(USER)
                .set(USER.PASSWORD,passwordEncoder.encode(randomPassword))
                .where(USER.NAME.eq(userName.username))
                .execute();

        PublicController.Message message = new PublicController.Message();
        message.message = "Password was reset to: " + randomPassword;

        return ResponseEntity.status(200).body(message);
    }

    @Secured({"ROLE_ADMINISTRATOR"})
    @PostMapping("/setRole")
    public ResponseEntity setRole(@Valid @RequestBody UserRole userRole) {
        Integer roleId = dbContext.select(ROLE.ID)
                .from(ROLE)
                .where(ROLE.NAME.eq(userRole.role))
                .fetchOneInto(Integer.class);

        dbContext.update(USER)
                .set(USER.ROLE, roleId)
                .where(USER.ID.eq(StaticMethods.getUserRecordByUsername(dbContext, userRole.username).getId()))
                .execute();

        return ResponseEntity.status(200).build();
    }

    private String getRandomPassword(){
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public static class UserRole{
        @NotBlank(message = "The username can not be empty")
        private String username;
        @NotBlank(message = "The role can not be empty")
        private String role;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }
    }

}
