const songContainer = document.querySelector('#songContainer');
const songCard = (songName,spotifySongId) => `<div class="card" style="width: 18rem;">
<div class="card-body">
  <h5 class="card-title">${songName}</h5>
  <iframe src="https://open.spotify.com/embed/track/${spotifySongId}" width="100%" height="80" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
  <br><br>
  <button class="btn btn-success" onclick="likeSong('${songName}')">👍</button>
  <button class="btn btn-danger" onclick="dislikeSong('${songName}')">👎</button>
</div>
</div>`

const songNameInput = document.querySelector('#songNameInput');
const songInterpretInput = document.querySelector('#songInterpretInput');
const spotifyLinkInput = document.querySelector('#spotifyLinkInput');
const suggestModal = document.querySelector('#songModal');

window.onload = () => {
    reloadSongSuggestions();
}


function suggestSong(){
    const url = '/songs';
    const params = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ "songName": `${songInterpretInput.value} - ${songNameInput.value}`, "spotifyLink": spotifyLinkInput.value })
    }
    fetch(url, params)
        .then(response => {
            response.json()
                .then((data) => {
                    if (response.status < 300) {
                        showAlert('Success', data.message || '', 'alert-success');
                        $('#songModal').modal('hide')
                        songNameInput.value = "";
                        spotifyLinkInput.value = "";
                    } else {
                        showAlert('Failed', data.message || '', 'alert-danger');
                    }
                })
                .catch((error) => {
                    if (response.status < 300) {
                        showAlert('Success', '', 'alert-success');
                        $('#songModal').modal('hide')
                        songNameInput.value = "";
                        spotifyLinkInput.value = "";
                    } else {
                        showAlert('Failed', '', 'alert-danger');
                    }
                })

        })
        .catch((error) => {
            console.log(error);
            return Promise.reject();
        });
}

function reloadSongSuggestions() {
    const url = '/songs';
    const params = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    }
    fetch(url, params)
        .then(response => {
            response.json()
                .then((data) => {
                    songContainer.innerHTML = "";
                  data.forEach(song => {
                      //https://open.spotify.com/track/0m7TguDgLue2LCaDF7B4qx?si=2Yuv8P3pSX2BS0DhWXqwlA

                      const startIndex = song.spotifyLink.indexOf("track/")+"track/".length;
                      const endIndex = song.spotifyLink.indexOf("?si=");
                      let spotifySongId = "";
                      if (endIndex < 0){
                        spotifySongId = song.spotifyLink.substring(startIndex);
                      }else{
                        spotifySongId = song.spotifyLink.substring(startIndex,endIndex);
                      }
                      songContainer.innerHTML += songCard(song.name, spotifySongId)
                  });
                })
                .catch((error) => {
                    if (response.status < 300) {
                        songNameInput.value = "";
                        spotifyLinkInput.value = "";
                    } else {
                        showAlert('Failed', "Wasn't able to load songs.", 'alert-danger');
                    }
                })

        })
        .catch((error) => {
            console.log(error);
            return Promise.reject();
        });
}

function likeSong(songName) {
    const url = '/rateSong';
    const params = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ 'songName': songName, positiveRating: true })
    }
    fetch(url, params)
        .then(response => {
            response.json()
                .then((data) => {
                    if (response.status < 300) {
                        showAlert('Success', data.message || '', 'alert-success');
                    } else {
                        showAlert('Failed', data.message || '', 'alert-danger');
                    }
                })
                .catch((error) => {
                    if (response.status < 300) {
                        showAlert('Success', '', 'alert-success');
                    } else {
                        showAlert('Failed', '', 'alert-danger');
                    }
                })
                reloadSongSuggestions();
        })
        .catch((error) => {
            console.log(error);
            return Promise.reject();
        });
}

function dislikeSong(songName) {
    const url = '/rateSong';
    const params = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ songName: songName, positiveRating: false })
    }
    fetch(url, params)
        .then(response => {
            response.json()
                .then((data) => {
                    if (response.status < 300) {
                        showAlert('Success', data.message || '', 'alert-success');
                    } else {
                        showAlert('Failed', data.message || '', 'alert-danger');
                    }
                })
                .catch((error) => {
                    if (response.status < 300) {
                        showAlert('Success', '', 'alert-success');
                    } else {
                        showAlert('Failed', '', 'alert-danger');
                    }
                })
                reloadSongSuggestions();
        })
        .catch((error) => {
            console.log(error);
            return Promise.reject();
        });
}