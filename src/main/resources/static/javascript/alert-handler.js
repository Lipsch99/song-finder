const alertObj = document.querySelector('.alert');
const alertBaseClasses = alertObj.className;
const alertHeader = alertObj.querySelector('.alert-heading');
const alertText = alertObj.querySelector('p');
alertObj.style.opacity = '0';

function showAlert(title, message, type) {
    alertObj.className = alertBaseClasses;
    alertObj.className += ` ${type}`;

    alertHeader.innerText = title;
    alertText.innerText = message;

    alertObj.style.opacity = '';
}

function hideAlert() {
    alertObj.style.opacity = '0';
}
