const userTable = document.querySelector('#userTable');
const songTable = document.querySelector('#songTable');
const userManagementDiv = document.querySelector('#user-management');
const userTableEntry = (name, role) => `<tr>
    <td>${name}</td>
    <input type="hidden" name="name" value="${name}">
    <td>
        <select class="custom-select" name="role">
            <option value="User" ${role == "User" ? "selected" : ""}>User</option>
            <option value="Moderator" ${role == "Moderator" ? "selected" : ""}>Moderator
            </option>
            <option value="Administrator" ${role == "Administrator" ? "selected" : ""}>
                Administrator</option>
        </select>
    </td>
    <td>
        <button type="button" class="btn btn-info" onclick='setRole(this,"${name}")'>Set Role</button>
    </td>
    <td><button type="button" class="btn btn-danger"
            onclick='resetPassword("${name}")'>Reset Password</button></td>
</tr>`;

const songTableEntry = (name, accepted) => `<tr>
<td>${name}</td>
<td><button type="button" class="btn btn-success ${accepted ? "disabled" : ""}"
        onclick='acceptSong("${name}")'>Accept</button></td>
<td><button type="button" class="btn btn-danger"
        onclick='deleteSong("${name}")'>Delete</button></td>
</tr>`;

const userSearchField = document.querySelector('#userSearchInput');
const songSearchField = document.querySelector('#songSearchInput')
userSearchField.onchange = updateUserTable;
songSearchField.onchange = updateSongTable;

let allUsers;
let allSongs;

fetchUsersFromDb().then(users => {
    allUsers = users;
    updateUserTable()
})
updateSongTable()

getRoleId().then(retrievedRoleId => {
    const roleId = retrievedRoleId.roleId;
    userManagementDiv.parentElement.removeChild(userManagementDiv);
})


async function fetchSongsFromDb() {
    const url = '/allSongs';
    const params = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    }
    return await fetch(url, params)
        .then(response => response.json())
        .then(data => {
            return data.sort((a, b) => {
                if (a.accepted == b.accepted) {
                    return 0;
                }
                if (a.accepted) {
                    return 1;
                }
                return -1;
            })
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

async function updateSongTable() {
    return await new Promise((resolve, reject) => {
        fetchSongsFromDb().then(songs => {
            allSongs = songs;
        })
            .then(() => {
                const filteredSongs = getFilteredSongs(songSearchField.value)
                clearSongTable();
                let i = 0;
                filteredSongs.forEach(song => {
                    if (i < 10) {
                        addNewSongEntry(song);
                    }
                    i++;
                });
                resolve();
            })
    })
}

function getFilteredSongs(name) {
    if (name == '') {
        return allSongs;
    }
    return allSongs.filter(x => x.name.includes(name));
}

function clearSongTable() {
    songTable.innerHTML = "";
}

function addNewSongEntry(song) {
    songTable.innerHTML += songTableEntry(song.name, song.accepted);
}


function addNewUserEntry(name, role) {
    userTable.innerHTML += userTableEntry(name, role);
}

function clearUserTable() {
    userTable.innerHTML = "";
}

async function fetchUsersFromDb() {
    const url = '/users';
    const params = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    }
    return await fetch(url, params)
        .then(response => response.json())
        .then(data => {
            return data;
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

function getFilteredUsers(name) {
    if (name == '') {
        return allUsers;
    }
    return allUsers.filter(x => x.username.includes(name));
}

function updateUserTable() {
    const filteredUsers = getFilteredUsers(userSearchField.value)
    clearUserTable();
    let i = 0;
    filteredUsers.forEach(user => {
        if (i < 10) {
            addNewUserEntry(user.username, user.role);
        }
        i++;
    });
}

function resetPassword(name) {
    const url = '/resetPassword';
    const params = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ "username": name })
    }
    fetch(url, params)
        .then(response => {
            response.json()
                .then((data) => {
                    if (response.status < 300) {
                        showAlert('Success', data.message || '', 'alert-success');
                    } else {
                        showAlert('Failed', data.message || '', 'alert-danger');
                    }
                })
                .catch((error) => {
                    if (response.status < 300) {
                        showAlert('Success', '', 'alert-success');
                    } else {
                        showAlert('Failed', '', 'alert-danger');
                    }
                })

        })
        .catch((error) => {
            console.log(error);
            return Promise.reject();
        });
}

function setRole($ele, name) {
    const selectedRole = $ele.parentElement.parentElement.querySelector('select[name="role"]').value;

    const url = '/setRole';
    const params = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ username: name, role: selectedRole})
    }
    fetch(url, params)
        .then(response => {
            response.json()
                .then((data) => {
                    if (response.status < 300) {
                        showAlert('Success', data.message || '', 'alert-success');
                    } else {
                        showAlert('Failed', data.message || '', 'alert-danger');
                    }
                })
                .catch((error) => {
                    if (response.status < 300) {
                        showAlert('Success', '', 'alert-success');
                    } else {
                        showAlert('Failed', '', 'alert-danger');
                    }
                })
            updateSongTable();
        })
        .catch((error) => {
            console.log(error);
            return Promise.reject();
        });
}

function acceptSong(songName) {
    const url = '/acceptSong';
    const params = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ name: songName, spotifyLink: "", accepted: false })
    }
    fetch(url, params)
        .then(response => {
            response.json()
                .then((data) => {
                    if (response.status < 300) {
                        showAlert('Success', data.message || '', 'alert-success');
                    } else {
                        showAlert('Failed', data.message || '', 'alert-danger');
                    }
                })
                .catch((error) => {
                    if (response.status < 300) {
                        showAlert('Success', '', 'alert-success');
                    } else {
                        showAlert('Failed', '', 'alert-danger');
                    }
                })
            updateSongTable();
        })
        .catch((error) => {
            console.log(error);
            return Promise.reject();
        });
}

function deleteSong(songName) {
    const url = '/deleteSong';
    const params = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ name: songName, spotifyLink: "", accepted: false })
    }
    fetch(url, params)
        .then(response => {
            response.json()
                .then((data) => {
                    if (response.status < 300) {
                        showAlert('Success', data.message || '', 'alert-success');
                    } else {
                        showAlert('Failed', data.message || '', 'alert-danger');
                    }
                })
                .catch((error) => {
                    if (response.status < 300) {
                        showAlert('Success', '', 'alert-success');
                    } else {
                        showAlert('Failed', '', 'alert-danger');
                    }
                })
            updateSongTable();
        })
        .catch((error) => {
            console.log(error);
            return Promise.reject();
        });
}

async function getRoleId(){
    const url = '/role';
    const params = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    }
    return await fetch(url, params)
        .then(response => response.json())
        .then(data => {
            return data;
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}