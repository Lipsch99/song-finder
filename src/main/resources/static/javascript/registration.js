function register() {
    const username = document.getElementById("username").value;
    const passwordA = document.getElementById("passwordA").value;
    const passwordB = document.getElementById("passwordB").value;

    if (passwordA != passwordB) {
        console.error("Passwords didn't match")
        return;
    }

    const url = '/register';
    const params = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ "username": username, "password": passwordA })
    }
    fetch(url, params)
        .then(response => {
            response.json()
                .then((data) => {
                    if (response.status < 300) {
                        showAlert('Success', data.message || '', 'alert-success');
                    } else {
                        showAlert('Failed', data.message || '', 'alert-danger');
                    }
                })
                .catch((error) => {
                    if (response.status < 300) {
                        showAlert('Success', '', 'alert-success');
                    } else {
                        showAlert('Failed', '', 'alert-danger');
                    }
                })

        })
        .catch((error) => {
            console.log(error);
            return Promise.reject();
        });
}