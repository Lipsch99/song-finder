const backOfficeMenu = document.querySelector('#navBar-back-office');
const voteEvaluationMenu = document.querySelector('#navBar-vote-evaluation');

let roleId = -1;
getRoleId().then(retrievedRoleId => {
    roleId = retrievedRoleId.roleId;

    if (roleId <= 1){
        backOfficeMenu.parentElement.removeChild(backOfficeMenu);
        voteEvaluationMenu.parentElement.removeChild(voteEvaluationMenu);
    }
})



async function getRoleId(){
    const url = '/role';
    const params = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    }
    return await fetch(url, params)
        .then(response => response.json())
        .then(data => {
            return data;
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}