const voteEntry = (songName, likeAmount, dislikeAmount, likePercentage) =>
    `<tr>
                                <td>${songName}</td>
                                <td>${likeAmount} 👍</td>
                                <td>${dislikeAmount} 👎</td>
                                <td>${likePercentage}%</td>
                            </tr>`

const mostVotedSongs = document.querySelector('#mostVotedSongs');
const bestRatedSongs = document.querySelector('#bestRatedSongs');

loadSongsData('/songs/mostVoted').then((data) => {
    updateSongsTable(mostVotedSongs, data);
})
loadSongsData('/songs/bestRated').then((data) => {
    updateSongsTable(bestRatedSongs, data);
})



async function loadSongsData(endpoint) {
    const url = endpoint;
    const params = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    }
    return new Promise((resolve, reject) => {
        fetch(url, params)
            .then(response => {
                response.json()
                    .then((data) => {
                        if (response.status >= 300) {
                            showAlert('Failed', data.message || '', 'alert-danger');
                        } else {
                            resolve(data);
                        }
                    })
                    .catch((error) => {
                        if (response.status >= 300) {
                            showAlert('Failed', data.message || '', 'alert-danger');
                        }
                    })
            })
            .catch((error) => {
                console.log(error);
                reject();
            });
    })
}

function updateSongsTable(table, data) {
    table.innerHTML = '';
    data.forEach(song => {
        table.innerHTML += voteEntry(song.name, song.likes, song.dislikes,
            getPercentageStringFromLikesAndDislikes(song.likes, song.dislikes));
    });
}

function getPercentageStringFromLikesAndDislikes(likes, dislikes) {
    return +(100 / (likes + dislikes) * likes).toFixed(2);
}
