const form = document.querySelector('form')
form.addEventListener('submit', changePassword);

function changePassword(event) {
    event.preventDefault();
    const formData = new FormData(form);
 
    const url = '/password';
    const params = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({"oldPassword": formData.get('oldPassword'), "newPassword": formData.get('newPassword')})
    }
    fetch(url, params)
    .then(response => {
        response.json()
            .then((data) => {
                if (response.status < 300) {
                    showAlert('Success', data.message || '', 'alert-success');
                } else {
                    showAlert('Failed', data.message || '', 'alert-danger');
                }
            })
            .catch((error) => {
                if (response.status < 300) {
                    showAlert('Success', '', 'alert-success');
                } else {
                    showAlert('Failed', '', 'alert-danger');
                }
            })

    })
    .catch((error) => {
        console.log(error);
        return Promise.reject();
    });
}