#!/bin/bash

DB_PASSWORD="Q33z9nHQNMQPKa6R"

CONTAINER_NAME=song_finder_db
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DATA_DIR="${CURRENT_DIR}/data"

echo "Data Dir: $DATA_DIR"
echo "$DATA_DIR":/var/lib/postgresql/data


docker volume create --name postgres-data-volume -d local
docker pull postgres
docker stop "$CONTAINER_NAME"
docker container rm "$CONTAINER_NAME"
docker run --name "$CONTAINER_NAME" -d -e POSTGRES_PASSWORD="$DB_PASSWORD" -p 5432:5432 -v "$DATA_DIR":/var/lib/postgresql/data postgres
#docker run --name "$CONTAINER_NAME" -d -e POSTGRES_PASSWORD="$DB_PASSWORD" -p 57200:5432 -v /var/lib/postgresql/data:/var/lib/postgresql/data postgres